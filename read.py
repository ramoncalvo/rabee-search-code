from itertools import takewhile
import xlrd
from tkinter import filedialog
import tkinter 
import xlsxwriter

import pygame
pygame.mixer.init(44100, -16,2,2048)

import os
print('Current work directory')
print(os.getcwd())

from tkinter.filedialog import askopenfilename, asksaveasfile, askdirectory


ventana = tkinter.Tk()
ventana.geometry("900x600")
etiqueta = tkinter.Label(ventana, text = "RaBee scan guides App", font = "Arial 20")
etiqueta.pack()
cajaTexto = tkinter.Entry(ventana, font = "Arial 30")
cajaTexto.pack()
cajaTexto.focus()

scanItems=0

ScannedGuides = []
MissingGuides = []


def column_len(sheet, index):
    col_values = sheet.col_values(index)
    col_len = len(col_values)
    for _ in takewhile(lambda x: not x, reversed(col_values)):
        col_len -= 1
    return col_len

def column_Data(sheet, index):
    col_values = sheet.col_values(index)
    return col_values

def openDialog():
    ventana.update()
    ventana.filename =  filedialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("xlsx files","*.xlsx"),("all files","*.*")))
    # print (ventana.filename)
    return ventana.filename


def searchByProduct():
    etiquetaResultado["text"] = 'No results'
    temporalNoResults = []
    searchableValue = cajaTexto.get()
    cajaTexto.delete(0,"end")
    for row in range(len(enterColumn)):
        if searchableValue == enterColumn[row]:
            etiquetaResultado["text"] = resultColumn[row]
            print(enterColumn[row])
            returnArrays(True)
            ScannedGuides.append(enterColumn[row])

            pygame.mixer.music.load("sounds/success.mp3") #Loading File Into Mixer
            pygame.mixer.music.play() #Playing It In The Whole Device
        else:
            temporalNoResults.append(False)
            if len(temporalNoResults) == len(enterColumn):
                returnArrays(False)
                pygame.mixer.music.load("sounds/error.mp3") #Loading File Into Mixer
                pygame.mixer.music.play() #Playing It In The Whole Device
                return MissingGuides.append(cajaTexto.get())
    
    
def returnArrays(value):
    if value == True:
        print('Scanned guides')
        print(ScannedGuides)
    else:
        print('Missing guides')
        print(MissingGuides)

def onEnter(event=None):
	searchByProduct()

def toWriteResult():

    directoryTosave = os.getcwd() + "/scanned_guides.xlsx"
    outWorkbook = xlsxwriter.Workbook(directoryTosave)
    outSheet = outWorkbook.add_worksheet()

    bold = outWorkbook.add_format({'bold': True})

    # write headers
    outSheet.write("A1", "Existing guides", bold)
    outSheet.write("B1", "Missing guides", bold)

    # write data to file
    for existing in range(len(ScannedGuides)):
        outSheet.write(existing+1, 0, ScannedGuides[existing])

    for missing in range(len(MissingGuides)):
        outSheet.write(missing+1, 1, MissingGuides[missing])

    resultScannedItems["text"] = 'The file is saved in: ' + os.getcwd() + "/scanned_guides.xlsx"

    outWorkbook.close()


def finishProccess():
    # print('finish process')
    toWriteResult()

# enterUser= 'YT2018611108000053'
# ./db.xlsx
# C:\db.xlsx

#book = xlrd.open_workbook("./db.xlsx")
book = xlrd.open_workbook(openDialog())
sheet = book.sheet_by_index(0)

# column 2 and 21
enterColumn=column_Data(sheet, 2)
resultColumn=column_Data(sheet, 21)
enterUser=cajaTexto.get()

ventana.bind('<Return>', onEnter)
boton1 = tkinter.Button(ventana, text = "Search guide", padx = 40, pady = 50, command = searchByProduct)
boton1.pack()

etiquetaResultado = tkinter.Label(ventana, font = "Arial 150")
etiquetaResultado.pack()

finishBtn = tkinter.Button(ventana, text = "Save result", padx = 40, pady = 50, command = finishProccess)
finishBtn.pack()

resultScannedItems = tkinter.Label(ventana, font = "Arial 18")
resultScannedItems.pack()

ventana.mainloop()